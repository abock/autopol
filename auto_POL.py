#!/usr/bin/env python

import numpy as np
import sys
#sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
#import dd_local as dd
#import ww_local as ww
from aug_sfutils import SFREAD, write_sf, getlastshot
from IPython import embed
#import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import signal
import time

def encoder2angle(time, data):
    data[data < np.average(data)] = data.min()
    data[data > np.average(data)] = data.max()
    data[data < np.average(data)] = 0
    data[data > np.average(data)] = 1

    dh2et = time[:-1]
    dh2ed = np.abs(np.diff(data))

    mask = dh2ed > 0.5
    masked_t = dh2et[mask]

    mask2 = np.diff(masked_t) > 2*np.average(np.diff(masked_t))
    indices = np.where(mask2)[0]
    to_add1 = (masked_t[indices]*1./3+masked_t[indices+1]*2./3.)
    to_add2 = (masked_t[indices]*2./3+masked_t[indices+1]*1./3.)
    ts = np.sort(np.concatenate((masked_t, to_add1, to_add2)))
    t2a = interp1d(ts, np.cumsum(len(ts)*[1]), bounds_error=False)
    t2a_old = interp1d(ts, np.cumsum(len(ts)*[1]) % 180, bounds_error=False)
    #plt.plot(ts, np.cumsum(len(ts)*[1]), '.-')
    segments = np.array([ts[0]] + list(ts[:-1][np.diff(t2a_old.y) < 0]))
    return t2a, segments

def butter_bandpass(lowcut, highcut, Fs, order=5):
    nyq = 0.5 * Fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, Fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, Fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y


if len(sys.argv) > 1:
    shot = int(sys.argv[1])
else:
    print('shotnumber argument missing')
    sys.exit()


shot = globals()['shot']
print(shot)



channels = [1, 2]

while 'con' not in globals():
    try:
        con = SFREAD('CON', shot)
    except:
        last_aug_shot = int(getlastshot())
        if last_aug_shot > shot:
            print("Could not find CON shot file but newer shot exists, skipping this one...")
            sys.exit()
        dt = 60
        print('CON not found, trying again in %is...'%dt)
        time.sleep(dt)

dets = [np.array(con.getobject('H%idet'%c)) for c in channels]
encs = [np.array(con.getobject('H%ienc'%c)) for c in channels]
t = np.array(con.gettimebase('H1det'))

angle_times, angles = [], []
for d, e in zip(dets, encs):
    t2a, segments = encoder2angle(t, e)

    min_f = np .min(1./np.diff(segments))
    max_f = np. max(1./np.diff(segments))
    df = 30

    fd = butter_bandpass_filter(d, min_f-df, max_f+df, 500e3, order=2)
    
    max_ind = signal.argrelextrema(fd, np.greater)[0]
    min_ind = signal.argrelextrema(fd, np.less)[0]


    angle_time = t[max_ind]
    angle = t2a(angle_time)
    
    #embed()
    angle[:len(segments[:len(angle)])] -= t2a(segments[:len(angle)])
    
    
    angle -= np.median(angle[angle_time < 0.05])
    angle *= -1
    
    cutstart = 3
    cutend = -3
    angle_times.append(angle_time[cutstart:cutend])
    angles.append(angle[cutstart:cutend])
    
data_d = {} 
for at, a, channel in zip(angle_times, angles, channels):
    data_d["timeH%i"%channel] = at.astype(np.float32)
    data_d["H%iangle"%channel] = a.astype(np.float32)

def write_ABOCK_POL_file():
    write_sf(shot, data_d, '.', 'POL', exp='ABOCK')


def write_AUGD_POL_file():
    write_sf(shot, data_d, '.', 'POL', exp='AUGD')


write_AUGD_POL_file()

#plt.xlim(0,0.015)




