#!/bin/zsh

#module load aug_sfutils

#cd /afs/ipp/home/a/abock/POL_DCN/POL/auto_POL

if [[ $1 = *"-"* ]]; then
  s0="${1[1,5]}"
  s1="${1[7,12]}"
  for s ({$s0..$s1})
     #echo $s
      #do /afs/ipp/.cs/anaconda/amd64_generic/2/4.2.0/bin/python ./auto_POL.py $s
      do python ./auto_POL.py $s
  done
  return
fi

for arg
    do
    #echo "/afs/ipp/.cs/anaconda/amd64_generic/2/4.2.0/bin/python ./auto_POL.py $arg"
    python ./auto_POL.py $arg
done
